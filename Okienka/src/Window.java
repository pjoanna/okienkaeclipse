import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JPanel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;
import javax.swing.border.MatteBorder;
import java.awt.Color;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;
import javax.swing.JButton;

public class Window {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Window window = new Window();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Window() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		frame.getContentPane().add(panel, gbc_panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel panel_left = new JPanel();
		panel_left.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.add(panel_left);
		GridBagLayout gbl_panel_left = new GridBagLayout();
		gbl_panel_left.columnWidths = new int[]{0, 0};
		gbl_panel_left.rowHeights = new int[]{0, 0, 0};
		gbl_panel_left.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_left.rowWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		panel_left.setLayout(gbl_panel_left);
		
		JPanel panel_lefttop = new JPanel();
		GridBagConstraints gbc_panel_lefttop = new GridBagConstraints();
		gbc_panel_lefttop.insets = new Insets(0, 0, 5, 0);
		gbc_panel_lefttop.fill = GridBagConstraints.BOTH;
		gbc_panel_lefttop.gridx = 0;
		gbc_panel_lefttop.gridy = 0;
		panel_left.add(panel_lefttop, gbc_panel_lefttop);
		GridBagLayout gbl_panel_lefttop = new GridBagLayout();
		gbl_panel_lefttop.columnWidths = new int[]{0, 0};
		gbl_panel_lefttop.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_panel_lefttop.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_lefttop.rowWeights = new double[]{0.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		panel_lefttop.setLayout(gbl_panel_lefttop);
		
		JLabel lblDaneFilmu = new JLabel("Dane filmu");
		GridBagConstraints gbc_lblDaneFilmu = new GridBagConstraints();
		gbc_lblDaneFilmu.insets = new Insets(0, 0, 5, 0);
		gbc_lblDaneFilmu.gridx = 0;
		gbc_lblDaneFilmu.gridy = 0;
		panel_lefttop.add(lblDaneFilmu, gbc_lblDaneFilmu);
		
		JPanel panel_tytul = new JPanel();
		GridBagConstraints gbc_panel_tytul = new GridBagConstraints();
		gbc_panel_tytul.insets = new Insets(0, 0, 5, 0);
		gbc_panel_tytul.fill = GridBagConstraints.BOTH;
		gbc_panel_tytul.gridx = 0;
		gbc_panel_tytul.gridy = 1;
		panel_lefttop.add(panel_tytul, gbc_panel_tytul);
		panel_tytul.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblTytu = new JLabel("Tytu\u0142");
		panel_tytul.add(lblTytu);
		
		JPanel panel_rezyser = new JPanel();
		GridBagConstraints gbc_panel_rezyser = new GridBagConstraints();
		gbc_panel_rezyser.insets = new Insets(0, 0, 5, 0);
		gbc_panel_rezyser.fill = GridBagConstraints.BOTH;
		gbc_panel_rezyser.gridx = 0;
		gbc_panel_rezyser.gridy = 2;
		panel_lefttop.add(panel_rezyser, gbc_panel_rezyser);
		panel_rezyser.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblReyser = new JLabel("Re\u017Cyser");
		panel_rezyser.add(lblReyser);
		
		JPanel panel_rok = new JPanel();
		panel_rok.setToolTipText("");
		GridBagConstraints gbc_panel_rok = new GridBagConstraints();
		gbc_panel_rok.insets = new Insets(0, 0, 5, 0);
		gbc_panel_rok.fill = GridBagConstraints.BOTH;
		gbc_panel_rok.gridx = 0;
		gbc_panel_rok.gridy = 3;
		panel_lefttop.add(panel_rok, gbc_panel_rok);
		panel_rok.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblRok = new JLabel("Rok");
		panel_rok.add(lblRok);
		
		JPanel panel_gatunek = new JPanel();
		GridBagConstraints gbc_panel_gatunek = new GridBagConstraints();
		gbc_panel_gatunek.insets = new Insets(0, 0, 5, 0);
		gbc_panel_gatunek.fill = GridBagConstraints.BOTH;
		gbc_panel_gatunek.gridx = 0;
		gbc_panel_gatunek.gridy = 4;
		panel_lefttop.add(panel_gatunek, gbc_panel_gatunek);
		panel_gatunek.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblGatunek = new JLabel("Gatunek");
		panel_gatunek.add(lblGatunek);
		
		JPanel panel_ocena = new JPanel();
		GridBagConstraints gbc_panel_ocena = new GridBagConstraints();
		gbc_panel_ocena.fill = GridBagConstraints.BOTH;
		gbc_panel_ocena.gridx = 0;
		gbc_panel_ocena.gridy = 5;
		panel_lefttop.add(panel_ocena, gbc_panel_ocena);
		panel_ocena.setLayout(new GridLayout(1, 0, 0, 0));
		
		JLabel lblOcena = new JLabel("Ocena");
		panel_ocena.add(lblOcena);
		
		JPanel panel_leftbottom = new JPanel();
		GridBagConstraints gbc_panel_leftbottom = new GridBagConstraints();
		gbc_panel_leftbottom.fill = GridBagConstraints.BOTH;
		gbc_panel_leftbottom.gridx = 0;
		gbc_panel_leftbottom.gridy = 1;
		panel_left.add(panel_leftbottom, gbc_panel_leftbottom);
		panel_leftbottom.setLayout(new GridLayout(0, 4, 0, 0));
		
		JButton dodaj = new JButton("dodaj");
		panel_leftbottom.add(dodaj);
		
		JButton usu� = new JButton("usu\u0144");
		panel_leftbottom.add(usu�);
		
		JButton zapisz = new JButton("zapisz");
		panel_leftbottom.add(zapisz);
		
		JButton odczytaj = new JButton("odczytaj");
		panel_leftbottom.add(odczytaj);
		
		JPanel panel_right = new JPanel();
		panel_right.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.add(panel_right);
	}

}
